// Count All Palindrome Sub-Strings in a String
// http://www.geeksforgeeks.org/count-palindrome-sub-strings-string/

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
using namespace std;

int f(const string & str) {
	int sz = str.size();
	if (sz == 0) {
		return 0;
	}
	vector<vector<bool>> A(sz, vector<bool>(sz, false));
	int result = 0;
	for (int len = 1; len <= sz; ++len) {
		for (int i = 0; i + len <= sz; ++i) {
			int j = i + len - 1;
			if (len == 1) {
				A[i][j] = true;
			}
			else if (len == 2) {
				if (str[i] == str[j]) {
					A[i][j] = true;
					++result;
				}
			}
			else {
				if (str[i] == str[j] and A[i + 1][j - 1]) {
					A[i][j] = true;
					++result;
				}
			}
		}
	}
	return result;
}

int main(void) {
	int T;
	cin >> T;
	while (T-- > 0) {
		int N;
		cin >> N;
		string str;
		cin >> str;
		cout << f(str) << '\n';
	}
	return 0;
}