// Longest Consecutive Subsequence
// http://www.geeksforgeeks.org/longest-consecutive-subsequence/
// http://practice.geeksforgeeks.org/problems/longest-consecutive-subsequence/0

#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_map>
using namespace std;

int f(vector<int> & a) {
	int result = 0;
	unordered_map<int, int> h;
	for (const auto & i : a) {
		if (h.count(i)) {
			continue;
		}
		int x = h.count(i - 1) ? h[i - 1] : 0;
		int y = h.count(i + 1) ? h[i + 1] : 0;
		int z = x + y + 1;
		result = max(result, z);
		h[i] = z;
		h[i - x] = z;
		h[i + y] = z;
	}
	return result;
}

int main(void) {
	int T, N, val;
	cin >> T;
	while (T-- > 0) {
		cin >> N;
		vector<int> a;
		for (int i = 0; i < N; ++i) {
			cin >> val;
			a.push_back(val);
		}
		cout << f(a) << '\n';
	}
	return 0;
}