// Distinct palindromic substrings

#include <iostream>
#include <vector>
#include <unordered_set>
using namespace std;

class Solution {
public:
	int f(string s) {
		int sz = s.size();
		if (sz <= 1) {
			return sz;
		}
		vector<vector<bool>> A(sz, vector<bool>(sz, false));
		unordered_set<string> B;
		for (int len = 1; len <= sz; ++len) {
			for (int i = 0; i + len <= sz; ++i) {
				int j = i + len - 1;
				if (len == 1) {
					A[i][j] = true;
					B.insert(s.substr(i, len));
				}
				else if (len == 2) {
					if (s[i] == s[j]) {
						A[i][j] = true;
						B.insert(s.substr(i, len));
					}
				}
				else {
					if (s[i] == s[j] and A[i + 1][j - 1]) {
						A[i][j] = true;
						B.insert(s.substr(i, len));
					}
				}
			}
		}
		return B.size();
	}
};

int main(void) {
	Solution solution;
	int T;
	cin >> T;
	while (T-- > 0) {
		string s;
		cin >> s;
		cout << solution.f(s) << '\n';
	}
	return 0;
}