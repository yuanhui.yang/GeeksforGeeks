// Check whether a given graph is Bipartite or not

bool isBipartite(int G[][MAX],int V) {
	vector<list<int>> A(V);
	for (int i = 0; i < V; ++i) {
		for (int j = 0; j < V; ++j) {
			if (G[i][j]) {
				if (i == j) {
					return false;
				}
				else {
					A[i].push_back(j);
				}
			}
		}
	}
	vector<int> B(V, -1);
	queue<int> curr;
	curr.push(0);
	B[0] = 0;
	while (!curr.empty()) {
		int front = curr.front();
		curr.pop();
		int color = B[front];
		for (const auto & i : A[front]) {
			if (B[i] < 0) {
				B[i] = 1 - color;
				curr.push(i);
			}
			else if (B[i] == color) {
				return false;
			}
		}
	}
	return true;
}