// How to check if a given number is Fibonacci number?
// http://www.geeksforgeeks.org/check-number-fibonacci-number/

/*
Given a number ‘n’, how to check if n is a Fibonacci number. First few Fibonacci numbers are 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 141, ..

Examples :

Input : 8
Output : Yes

Input : 34
Output : Yes

Input : 41
Output : No
Recommended: Please solve it on “PRACTICE ” first, before moving on to the solution.
A simple way is to generate Fibonacci numbers until the generated number is greater than or equal to ‘n’. Following is an interesting property about Fibonacci numbers that can also be used to check if a given number is Fibonacci or not.
A number is Fibonacci if and only if one or both of (5*n2 + 4) or (5*n2 – 4) is a perfect square (Source: Wiki). Following is a simple program based on this concept.
*/

#include <iostream>
#include <cmath>

using namespace std;

class Solution {
public:
	bool isFibonacci(long long n) {
		return f(5LL * n * n - 4LL) or f(5LL * n * n + 4LL);
	}
private:
	bool f(long long n) {
		long long m = sqrt(n);
		return m * m == n;
	}
};

int main(void) {
	Solution solution;
	int t;
	cin >> t;
	while (t-- > 0) {
		long long n;
		cin >> n;
		if (solution.isFibonacci(n)) {
			cout << "Yes\n";
		}
		else {
			cout << "No\n";
		}
	}
	return 0;
}

#include <iostream>
#include <cmath>

using namespace std;

class Solution {
public:
	bool isFibonacci(long long n) {
		long long prev = 1, curr = 1;
		while (curr < n) {
			long long t = prev;
			prev = curr;
			curr += t;
		}
		return curr == n;
	}
};

int main(void) {
	Solution solution;
	int t;
	cin >> t;
	while (t-- > 0) {
		long long n;
		cin >> n;
		if (solution.isFibonacci(n)) {
			cout << "Yes\n";
		}
		else {
			cout << "No\n";
		}
	}
	return 0;
}