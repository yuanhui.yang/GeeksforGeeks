# GeeksforGeeks
www.geeksforgeeks.org

| Title | Solution |
| --- | --- |
| [Find all distinct palindromic sub-strings of a given string](http://www.geeksforgeeks.org/find-number-distinct-palindromic-sub-strings-given-string/) | [C++](https://github.com/yuanhui-yang/GeeksforGeeks/blob/master/distinct-palindromic-substrings.cpp.cpp) |
| [Check whether a given graph is Bipartite or not](http://www.geeksforgeeks.org/bipartite-graph/) | [C++](validate-bipartite-graph.cpp) |
| [Count All Palindrome Sub-Strings in a String](http://www.geeksforgeeks.org/count-palindrome-sub-strings-string/) | [C++](https://github.com/yuanhui-yang/GeeksforGeeks/blob/master/count-palindrome-sub-strings-string.cpp) |
| [Verify Fibonacci Number](http://www.geeksforgeeks.org/check-number-fibonacci-number/) | [C++](https://github.com/yuanhui-yang/GeeksforGeeks/blob/master/verify-fibonacci-number.cpp) |
| [Number of occurrences of 2 as a digit in numbers from 0 to n](http://www.geeksforgeeks.org/number-of-occurrences-of-2-as-a-digit-in-numbers-from-0-to-n/) | [C++](https://github.com/yuanhui-yang/GeeksforGeeks/blob/master/number-of-occurrences-of-2-as-a-digit-in-numbers-from-0-to-n.cpp) |
| [Longest Consecutive Subsequence](http://www.geeksforgeeks.org/longest-consecutive-subsequence/) | [C++](https://github.com/yuanhui-yang/GeeksforGeeks/blob/master/longest-consecutive-subsequence.cpp) |
| [QuickSort](http://www.geeksforgeeks.org/quick-sort/) | [C++](https://github.com/yuanhui-yang/GeeksforGeeks/blob/master/quick-sort.cpp) |
| [Largest Sum Contiguous Subarray](http://www.geeksforgeeks.org/largest-sum-contiguous-subarray/) | [C++](https://github.com/yuanhui-yang/GeeksforGeeks/blob/master/largest-sum-contiguous-subarray.cpp) |
| [Longest Common Subsequence](http://www.geeksforgeeks.org/dynamic-programming-set-4-longest-common-subsequence/) | [C++](https://github.com/yuanhui-yang/GeeksforGeeks/blob/master/longest-common-subsequence.cpp) |
| [Finding middle element in a linked list](http://www.geeksforgeeks.org/write-a-c-function-to-print-the-middle-of-the-linked-list/) | [C++](https://github.com/yuanhui-yang/GeeksforGeeks/blob/master/finding-middle-element-in-a-linked-list.cpp) |
| [Count number of binary strings without consecutive 1's](http://www.geeksforgeeks.org/count-number-binary-strings-without-consecutive-1s/) | [C++](https://github.com/yuanhui-yang/GeeksforGeeks/blob/master/count-number-binary-strings-without-consecutive-1s.cpp) |
