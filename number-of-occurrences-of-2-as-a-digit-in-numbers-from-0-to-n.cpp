// Number of occurrences of 2 as a digit in numbers from 0 to n
// http://www.geeksforgeeks.org/number-of-occurrences-of-2-as-a-digit-in-numbers-from-0-to-n/

/*
Count the number of 2s as digit in all numbers from 0 to n.

Input:
The first line of input contains an integer T denoting the number of test cases. Then T test cases follow. Each test case contains the input integer n.

Output:
Print the count of the number of 2s as digit in all numbers from 0 to n.

Constraints:
1<=T<=100
1<=N<=10^5

Example:
Input:
2
22
100

Output:
6
20
*/

/* Counts the number of 2s in a number at d-th
   digit */
long long int count2sinRangeAtDigit(long long int number, long long int d)
{
// Your code goes here
	long long int x = pow(10LL, d), y = x * 10LL;
	return number / y * x + min(x, max(0LL, 1LL + number % y - 2LL * x));
}
/* Counts the number of '2' digits between 0 and n */
long long int numberOf2sinRange(long long int number)
{
// Your code goes here
	long long int result = 0;
	for (long long int d = log10(number), i = 0; i <= d; ++i) {
		result += count2sinRangeAtDigit(number, i);
	}
	return result;
}