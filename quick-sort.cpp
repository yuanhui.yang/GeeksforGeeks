// QuickSort
// http://www.geeksforgeeks.org/quick-sort/
// http://practice.geeksforgeeks.org/problems/sort-the-array/0

#include <iostream>

using namespace std;

void swap(int * a, int * b) {
    int c = *a;
    *a = *b;
    *b = c;
}

int partition(int * nums, int p, int q) {
    int pivot = nums[p], i = p + 1, j = q;
    while (i <= j) {
        if (nums[i] <= pivot) {
            ++i;
            continue;
        }
        if (nums[j] > pivot) {
            --j;
            continue;
        }
        swap(&nums[i++], &nums[j--]);
    }
    swap(&nums[p], &nums[j]);
    return j;
}

void quickSort(int * nums, int p, int r) {
    if (p < r) {
        int q = partition(nums, p, r);
        quickSort(nums, p, q - 1);
        quickSort(nums, q + 1, r);
    }
}


int main(void) {
    int T, N, nums[1000];
    cin >> T;
    while (T-- > 0) {
        cin >> N;
        for (int i = 0; i < N; ++i) {
            cin >> nums[i];
        }
        quickSort(nums, 0, N - 1);
        for (int i = 0; i < N; ++i) {
            cout << nums[i] << ' ';
        }
        cout << '\n';
    }
    return 0;
}